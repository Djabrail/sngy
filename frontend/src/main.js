import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from './plugins/vuetify'
import apolloProvider from './vue-apollo'

new Vue({
  router,
  vuetify,
  apolloProvider: apolloProvider,
  render: (h) => h(App),
}).$mount("#app");
