import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.css'

Vue.use(Vuetify)

export default new Vuetify({
    icons: {
      iconfont: 'mdi',
    },
    theme: {
      themes: {
        light: {
          primary: "#ffcc50",
          error: "#ff8888",
          success: "#45a5ae",

          secondary: '#b0bec5',
          accent: '#8c9eff',
        },
        dark: {
          primary: "#ffcc50",
          error: "#ff8888",
          success: "#45a5ae",

          secondary: '#b0bec5',
          accent: '#8c9eff',

        },
      },
    },

  });


