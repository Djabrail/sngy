from graphene_django import DjangoObjectType
import graphene

from .models import Occupation


class OccupationType(DjangoObjectType):

    class Meta:
        model = Occupation
        fields = ("id", "name", "company_name", "position_name", "hire_date", "fire_date", "salary", "fraction", "base",
                  "advance", "by_hours")


class Query(graphene.ObjectType):
    get_occupations = graphene.List(OccupationType)
    get_occupation = graphene.Field(OccupationType, occupationId=graphene.Int())

    def resolve_get_occupations(self, info):
        return Occupation.objects.all()

    def resolve_get_occupation(self, info, occupationId):
        return Occupation.objects.get(pk=occupationId)


class Mutation(graphene.ObjectType):
    add_occupation = graphene.Field(OccupationType,
                                    id=graphene.ID(),
                                    name=graphene.String(required=True),
                                    company_name=graphene.String(required=True),
                                    position_name=graphene.String(required=True),
                                    hire_date=graphene.Date(required=True),
                                    fire_date=graphene.Date(),
                                    salary=graphene.Int(required=True),
                                    fraction=graphene.Int(required=True),
                                    base=graphene.Int(required=True),
                                    advance=graphene.Int(required=True),
                                    by_hours=graphene.Boolean(required=True)
                                    )

    update_occupation = graphene.Field(OccupationType,
                                    occupationId=graphene.ID(),
                                    salary=graphene.Int(required=True),
                                    fraction=graphene.Int(required=True),
                                    base=graphene.Int(required=True),
                                    advance=graphene.Int(required=True),
                                    by_hours=graphene.Boolean(required=True)
                                    )

    def resolve_add_occupation(self, info, **kwargs):
        return Occupation.objects.create(**kwargs)

    def resolve_update_occupation(self, info, occupationId, **kwargs):
        occupation = Occupation.objects.get(id=occupationId)
        occupation.salary = kwargs['salary']
        occupation.fraction = kwargs['fraction']
        occupation.base = kwargs['base']
        occupation.advance = kwargs['advance']
        occupation.by_hours = kwargs['by_hours']
        occupation.save()
        return occupation


schema = graphene.Schema(query=Query, mutation=Mutation)
