from django.db import models

class Occupation(models.Model):
    name = models.CharField('Имя', max_length=200)
    company_name = models.CharField('Компания', max_length=200)
    position_name = models.CharField('Должность', max_length=200)
    hire_date = models.DateField('Дата приёма')
    fire_date = models.DateField('Дата увольнения', blank=True, null=True)
    salary = models.IntegerField('Ставка, руб.')
    fraction = models.IntegerField('Ставка, %')
    base = models.IntegerField('База, руб.')
    advance = models.IntegerField('Аванс, руб.')
    by_hours = models.BooleanField('Почасовая оплата', default=False)

    def __str__(self):
        return self.name