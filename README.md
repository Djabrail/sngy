# Backend

## Install dependencies
```
pip install -r requirements.txt
```

### Run the project
```
python manage.py runserver
```

# Frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run start
```

